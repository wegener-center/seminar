# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError, NON_FIELD_ERRORS

# Create your models here.
@python_2_unicode_compatible
class Location(models.Model):
    name = models.CharField(
        max_length=200, verbose_name="location name",
        help_text='e.g. SR Wegener Center')
    address = models.CharField(
        max_length=200, verbose_name="full address",
        blank=True, help_text='e.g. Brandhofgasse 5, 1st floor')

    def __str__(self):
        if self.address:
            return u"{}, {}".format(self.name, self.address)
        else:
            return self.name

@python_2_unicode_compatible
class FullNameUser(User):
    class Meta:
        proxy = True

    def __str__(self):
        return self.get_full_name()
    
@python_2_unicode_compatible
class Series(models.Model):
    name = models.CharField(max_length=200, verbose_name="series name",
                            help_text='e.g. T4S')
    responsible = models.ForeignKey(
        FullNameUser, verbose_name='person in charge',
        related_name='personincharge+',
        help_text="warning mails are sent to the user's corresponging address")
    sender = models.EmailField(
        verbose_name='from address', blank=True,
        help_text="if not set email is sent from the person's in charge address")

    class Meta:
        verbose_name_plural = 'Series'
        ordering = ['name']
        
    def __str__(self):
        return self.name

@python_2_unicode_compatible
class Presenter(models.Model):
    first_name = models.CharField(max_length=200, verbose_name='first name')
    last_name = models.CharField(max_length=200, verbose_name='last name')
    affiliation = models.CharField(max_length=200, verbose_name='affiliation', blank=True)
    biography = models.TextField(verbose_name='short biography', blank=True,
                                 help_text='use blank lines to start new paragraphs')

    class Meta:
        ordering = ('last_name',)
        
    def full_name(self):
        return u"{} {}".format(self.first_name, self.last_name)

    def __str__(self):
        return self.full_name()

class CreatedModifiedByAtModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, related_name='created+')
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, related_name='modified+')

    class Meta:
        abstract = True
        
@python_2_unicode_compatible
class Presentation(CreatedModifiedByAtModel):
    presenter = models.ForeignKey(Presenter, blank=True, null=True,
                                  help_text='leave this blank if presenter is not known')
    title = models.CharField(max_length=200, verbose_name='title', blank=True,
                             help_text='leave this blank until you know the title')
    moderator = models.CharField(max_length=100, verbose_name='moderator', blank=True)
    abstract = models.TextField(verbose_name='abstract', blank=True)
    date = models.DateTimeField(verbose_name='start time')
    duration = models.DurationField(verbose_name='duration', default="01:30:00",
                                    help_text='HH:MM:SS')
    series = models.ForeignKey(Series)
    location = models.ForeignKey(Location)
    
    class Meta:
        ordering = ('date',)
        get_latest_by = 'date'
        
    def __str__(self):
        title = self.title or 't.b.d'
        presenter = self.presenter or 't.b.d'
        return u"{} (by {})".format(title, presenter)

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse('presentation-detail', args=[str(self.id)])
    
    def validate_unique(self, *args, **kwargs):
        # time interval and location must be unique together
        super(Presentation, self).validate_unique(*args, **kwargs)

        start_time = self.date
        end_time = self.date + self.duration

        # check time-span for all objects at this location
        # excluding the object itself
        prs = self.__class__._default_manager.filter(location=self.location)
        if self.pk:
            prs = prs.exclude(pk=self.pk)
            
        for pr in prs:
            if start_time < pr.date < end_time \
               or start_time < pr.date+pr.duration < end_time:
                error_msgs = [
                    "There is already another presentation scheduled:",
                    "{:%F %H:%M}–{:%H:%M} {}".format(
                        pr.date, pr.date+pr.duration, pr.location),
                    pr
                ]
                raise ValidationError(
                    {NON_FIELD_ERRORS: [error_msgs],
                     'date': ValidationError('', ''),
                     'duration': ValidationError('', ''),
                     'location': ValidationError('', '')})

    
@python_2_unicode_compatible
class Recipient(models.Model):
    name = models.CharField(verbose_name="Name", max_length=100, blank=True)
    email = models.EmailField(verbose_name="E-mail", unique=True)

    class Meta:
        ordering = ('email',)

    def __str__(self):
        if self.name:
            return u"{} <{}>".format(self.name, self.email)
        else:
            return self.email


@python_2_unicode_compatible
class Notification(models.Model):
    series = models.ForeignKey(Series)
    recipients = models.ManyToManyField(
        Recipient, blank=True,
        verbose_name='Recipients that will be informed')
    reminder = models.PositiveSmallIntegerField(
        verbose_name='Reminder', help_text='Number of days before the '
        'presentation when reminders are send out by email')

    class Meta:
        ordering = ('series', '-reminder')

    def __str__(self):
        number = self.recipients.count()
        return u"{} - {} day{} ahead ({} recipient{})".format(
            self.series, self.reminder, self.reminder != 1 and 's' or '',
            number, number != 1 and 's' or ''
        )


@python_2_unicode_compatible
class ReminderSent(models.Model):
    presentation = models.ForeignKey(Presentation)
    notification = models.ForeignKey(Notification)
    sent = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = (('presentation', 'notification'),)
        ordering = ('sent',)
        get_latest_by = 'sent'
        verbose_name = 'Reminder sent'
        verbose_name_plural = 'Reminders sent'
        
    def __str__(self):
        return u"{} {} {}".format(
            self.presentation, self.notification, self.sent)
