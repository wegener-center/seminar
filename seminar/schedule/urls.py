from django.conf.urls import url
from .views import PresentationList, PresentationDetail, ical_export

urlpatterns = [
    url(r'^$', PresentationList.as_view(), {'list': 'upcoming'}),
    url(r'^past$', PresentationList.as_view(), {'list': 'past'}),
    url(r'^(?P<slug>[a-z][a-z\-0-9]+)/$', PresentationList.as_view(), {'list': 'upcoming'}),
    url(r'^(?P<slug>[a-z][a-z\-0-9]+)/past$', PresentationList.as_view(), {'list': 'past'}),
    url(r'^(?P<pk>\d+)/$', PresentationDetail.as_view(), name="presentation-detail"),
    url(r'^export/(?P<pk>\d+)/$', ical_export, name="ical-export"),
]
