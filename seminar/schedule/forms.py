# -*- coding: utf-8 -*-

from django import forms
from django.forms import ModelChoiceField, ModelMultipleChoiceField
from django.contrib.auth import get_user_model
from .models import Series

class UserModelChoiceField(ModelChoiceField):
    """
    A ModelChoiceField to represent User 
    select boxes in the Auto Admin 
    """
    def label_from_instance(self, obj):
        return obj.get_full_name()
    
class UserModelMultipleChoiceField(ModelMultipleChoiceField):
    """
    Similar to UserModelChoiceField, provide a nicer-looking 
    list of user names for ManyToMany Relations...
    """
    def label_from_instance(self, obj):
        return obj.get_full_name()

class SeriesAdminForm(forms.ModelForm):
    responsible = UserModelChoiceField(
        get_user_model().objects \
        .filter(is_superuser=False)      \
        .order_by('first_name', 'last_name'))
    
    class Meta:
        model = Series
        fields = '__all__'
