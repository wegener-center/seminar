from django.core.management.base import BaseCommand, CommandError
from django.template import loader
from django.core.mail import EmailMultiAlternatives
from django.contrib.sites.models import Site
from django.conf import settings

from schedule.models import Presentation, Notification, Recipient, ReminderSent

from dateutil.parser import parse
import datetime

class Command(BaseCommand):
    help = "Send out notification (warning) mails"

    def add_arguments(self, parser):
        parser.add_argument(
            '--dry-run', action='store_true', dest='test', default=False,
            help='do not send out emails but print the actions on console')
        parser.add_argument(
            '--date', dest='date', type=str,
            help='simulated run date (that can somehow be parsed)'
            ' will be ingnored in non dry-run'
        )
        
    def handle(self, *args, **options):
        date = datetime.date.today()
        if options['test']:
            if options['date']:
                try:
                    date = parse(options['date']).date()
                except ValueError:
                    try:
                        datestring = u'"{}"'.format(options['date'])
                    except:
                        datestring = 'the given string'
                    raise CommandError(u'{} is not a valid date'
                                       .format(datestring))

        # load templates and set common context
        subject = loader.get_template('schedule/reminder_subject.txt')
        text = loader.get_template('schedule/reminder_mail.txt')
        html = loader.get_template('schedule/reminder_mail.html')
        warning = loader.get_template('schedule/warning_mail.txt')

        context = {'site': Site.objects.get_current()}

        tba = dict()  # to be announced dictionary
        # loop over all notifications
        for notification in Notification.objects.all():
            # get all presentations that lie within date and notification days
            notification_sent = ReminderSent \
                                .objects \
                                .filter(notification=notification) \
                                .values_list('presentation', flat=True)
            presentations = Presentation \
                            .objects \
                            .filter(
                                series=notification.series,
                                date__date__range=(
                                    date,
                                    date+datetime.timedelta(
                                        notification.reminder))) \
                            .exclude(id__in=notification_sent)
            
            for pr in presentations:
                tba.setdefault(pr, []).append(notification)

        for pr, notifications in tba.items():
            context.update({'presentation': pr})
            mail_subject = subject.render(context).strip()
            
            # check if announcement of presentation can be sent out
            if not (pr.title and pr.presenter):
                # send out warning
                from_addr = settings.ADMIN_EMAIL
                recipients = [u'{} <{}>'.format(pr.series.responsible,
                                                pr.series.responsible.email)]
                mail_body = warning.render(context)
                mail_html = None

            else:
                # sender address
                from_addr = pr.series.sender if pr.series.sender else \
                            u'{} <{}>'.format(pr.series.responsible,
                                              pr.series.responsible.email)
            
                # get all recipients' email-addresses
                recipients = Recipient.objects.select_related().filter(
                    notification__id__in=[ x.id for x in notifications ]).distinct()
                
                mail_body = text.render(context)
                mail_html = html.render(context)

                # if not in dry-run save the date when mail was sent
                if not options['test']:
                    for notification in notifications:
                        rs = ReminderSent(presentation=pr, notification=notification)
                        rs.save()

                    
            # output either by mail or on console    
            if options['test']:
                self.stdout.write(u'Email sent by {}'.format(from_addr))
                self.stdout.write(u'To: {}'.format([ x.__str__() for x in recipients ]))
                self.stdout.write(u'Subject: {}'.format(mail_subject))
                self.stdout.write(u'{}'.format(mail_body))
                self.stdout.write('\n')

            else:
                # send to all recipients as mass mail
                msg = EmailMultiAlternatives(mail_subject, mail_body, from_addr, recipients)
                if mail_html:
                    msg.attach_alternative(mail_html, 'text/html')
                msg.send()
            
