# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import (
    Presentation, Presenter, Series, Location,
    Notification, Recipient, ReminderSent
)
from .forms import SeriesAdminForm

# Register your models here.

# do not show a model in the overview
class HideModelAdmin(admin.ModelAdmin):
    def get_model_perms(self, request):
        return {}
    
admin.site.register(Presenter, HideModelAdmin)
admin.site.register(Location, HideModelAdmin)
    
class SeriesAdmin(admin.ModelAdmin):
    form = SeriesAdminForm
    fieldsets = [
        ('Standard info', {'fields': ['name']},),
        ('Contact', {'fields': ['responsible', 'sender']})
    ]
    list_display = ('name', 'responsible')
    search_fields = ('name', 'responsible__first_name', 'responsible__last_name')
    list_filter = ('responsible',)

    
admin.site.register(Series, SeriesAdmin)


class ReadyToPublishFilter(admin.SimpleListFilter):
    """
    filter presentations with missing data
    """
    title = 'ready to publish'
    parameter_name = 'missing'
    
    def lookups(self, request, model_admin):
        return (
            ('ok', 'ok—info complete'),
            ('minor', 'missing abstract'),
            ('critical', 'missing basics'),
        )
    def queryset(self, request, queryset):
        if self.value() == 'ok':
            return queryset \
                .exclude(presenter__isnull=True) \
                .exclude(title__exact='')        \
                .exclude(abstract__exact='')
        elif self.value() == 'minor':
            return queryset \
                .exclude(presenter__isnull=True) \
                .exclude(title__exact='')
        elif self.value() == 'critical':
            return queryset.filter(presenter__isnull=True) \
                | queryset.filter(title__exact='')
        
class PresentationAdmin(admin.ModelAdmin):
    date_hierarchy = 'date'
    readonly_fields = ('created_by', 'created_at', 'modified_by', 'modified_at')
    fieldsets = [
        ('Presenter', {'fields': ['presenter']}),
        ('Presentation', {'fields': ['title', 'abstract', 'moderator']}),
        ('Time & Location', {'fields': [('date', 'duration'), 'location'],
                             'description': '<span class="help">ATTENTION: THIS IS NO BOOKING TOOL—PLEASE USE TINE2.0</span>'}),
        ('Series', {'fields': ['series']}),
        ('Admin info', {'fields': [('created_by', 'created_at'),
                                   ('modified_by', 'modified_at')]})
    ]
    list_display = ('date', 'presenter', 'title', 'series',)
    search_fields = ('presenter__first_name', 'presenter__last_name',
                     'title', 'abstract')

    list_filter = (ReadyToPublishFilter,
                   'presenter', 'presenter__affiliation', 'location',
                   'date', 'series', 'created_by', 'modified_by')

    # save the created_by and modified_by information
    def save_model(self, request, obj, form, change):
        user = request.user
        obj.modified_by = user
        if not change:
            obj.created_by = user
        obj.save()
        
admin.site.register(Presentation, PresentationAdmin)

class RecipientAdmin(admin.ModelAdmin):
    list_display = ('email', 'name')
    search_fields = ('name', 'email')

admin.site.register(Recipient, RecipientAdmin)

class NotificationAdmin(admin.ModelAdmin):
    list_display = ('series', 'reminder', 'no_recipients')
    list_filter = ('series',)
    filter_horizontal = ('recipients',)
    fieldsets = [
        ('Basics', {'fields': ['series', 'reminder']}),
        ('Recipients', {'fields': ['recipients']})
    ]
    
    def no_recipients(self, obj):
        return obj.recipients.count()
    no_recipients.short_description = 'Number of recipients'

admin.site.register(Notification, NotificationAdmin)

class ReminderSentAdmin(admin.ModelAdmin):
    list_display = ('presentation', 'notification', 'sent')
    list_filter = ('notification__series', 'presentation')

    readonly_fields = ('presentation', 'notification', 'sent')
    
    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permisssion(self, request, obj=None):
        return False

admin.site.register(ReminderSent, ReminderSentAdmin)
