# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-10-12 11:15
from __future__ import unicode_literals

import django.contrib.auth.models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0008_alter_user_username_max_length'),
        ('schedule', '0010_auto_20161003_1416'),
    ]

    operations = [
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('reminder', models.PositiveSmallIntegerField(help_text='Number of days before the presentation when reminders are send out by email', verbose_name='Reminder [days]')),
            ],
            options={
                'ordering': ('series', '-reminder'),
            },
        ),
        migrations.CreateModel(
            name='Recipient',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=100, verbose_name='Name')),
                ('email', models.EmailField(max_length=254, verbose_name='E-mail')),
            ],
            options={
                'ordering': ('email',),
            },
        ),
        migrations.CreateModel(
            name='FullNameUser',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('auth.user',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.AlterField(
            model_name='series',
            name='responsible',
            field=models.ForeignKey(help_text="warning mails are sent to the user's corresponging address", on_delete=django.db.models.deletion.CASCADE, related_name='personincharge+', to='schedule.FullNameUser', verbose_name='person in charge'),
        ),
        migrations.AlterField(
            model_name='series',
            name='sender',
            field=models.EmailField(blank=True, help_text="if not set email is sent from the person's in charge address", max_length=254, verbose_name='from address'),
        ),
        migrations.AddField(
            model_name='notification',
            name='recipients',
            field=models.ManyToManyField(to='schedule.Recipient'),
        ),
        migrations.AddField(
            model_name='notification',
            name='series',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='schedule.Series'),
        ),
    ]
