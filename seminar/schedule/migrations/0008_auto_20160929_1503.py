# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-09-29 13:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0007_presenter_extra'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='presentation',
            options={'ordering': ('date',)},
        ),
        migrations.AlterModelOptions(
            name='presenter',
            options={'ordering': ('last_name',)},
        ),
        migrations.RemoveField(
            model_name='presenter',
            name='name',
        ),
        migrations.AddField(
            model_name='presenter',
            name='first_name',
            field=models.CharField(max_length=200, verbose_name='first name'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='presenter',
            name='last_name',
            field=models.CharField(max_length=200, verbose_name='last name'),
            preserve_default=False,
        ),
    ]
