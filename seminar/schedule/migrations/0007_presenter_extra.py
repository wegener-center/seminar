# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-09-29 12:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0006_auto_20160928_1524'),
    ]

    operations = [
        migrations.AddField(
            model_name='presenter',
            name='extra',
            field=models.TextField(blank=True, help_text='extra information on presenter, e.g. CV', verbose_name='extra info'),
        ),
    ]
