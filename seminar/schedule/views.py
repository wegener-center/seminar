from django.views.generic import ListView, DetailView
from django.utils import timezone
from django.utils.text import slugify
from icalendar import Calendar, Event, vCalAddress, vText
from django.http import HttpResponse
from django.shortcuts import get_object_or_404

from .models import Presentation, Series

# Create your views here.
class PresentationList(ListView):
    def get_queryset(self):
        this_morning = timezone.now().replace(hour=1, minute=0, second=0)

        if 'list' in self.kwargs and self.kwargs['list'].startswith('past'):
            queryset = Presentation.objects \
                                   .filter(date__lt=this_morning) \
                                   .order_by('-date')
        else:
            queryset = Presentation.objects \
                                   .filter(date__gte=this_morning)
        queryset = queryset.exclude(presenter__isnull=True) \
                           .exclude(title__exact='')

        # check if a valid slug is given
        if 'slug' in self.kwargs and self.kwargs['slug']:
            series = [ x for x in Series.objects.all() \
                       if slugify(x) == self.kwargs['slug'] ]
            if series:
                queryset = queryset.filter(series__in=series)
            else:
                queryset = Presentation.objects.none()
                
        self.series = sorted(list(set([ x.series for x in queryset ])), key=lambda x: x.name)

        return queryset


    def get_context_data(self, **kwargs):
        context = super(PresentationList, self).get_context_data(**kwargs)
        context.update(self.kwargs)
        context['series'] = self.series
        return context


class PresentationDetail(DetailView):
    queryset = Presentation.objects \
                           .exclude(presenter__isnull=True) \
                           .exclude(title__exact='')


def ical_export(request, pk):
    pr = get_object_or_404(Presentation, pk=pk)

    cal = Calendar()
    cal.add('prodid', '-//{} Presentation Calendar//'.format(pr.series))
    cal.add('verions', '2.0')

    ical_event = Event()
    ical_event.add('summary', pr)
    ical_event.add('dtstart', pr.date)
    ical_event.add('dtend', pr.date + pr.duration)
    ical_event.add('dtstamp', pr.created_at)
    ical_event.add('description', pr.abstract)
    organizer = vCalAddress('MAILTO:{}'.format(pr.series.responsible.email))
    organizer.params['cn'] = vText(pr.series.responsible.get_full_name())
    organizer.params['role'] = vText('CHAIR')
    ical_event['organizer'] = organizer
    ical_event['location'] = vText(pr.location)
    ical_event['uid'] = 'presentation-{}.series-{}'.format(pr.id, pr.series.id)

    cal.add_component(ical_event)

    response = HttpResponse(cal.to_ical(), content_type='text/calendar')
    response['Content-Disposition'] = 'attachment; filename="{}.ics'.format(slugify(pr.series))

    return response
